/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	

	
	//first function here:

	function identityPrompt(){
		let fullNameOfPerson = prompt("Enter you Full Name: ");
		let ageOfPerson = prompt("Enter Your Age: " );
		let locationOfPerson = prompt("Enter your location: ");

		console.log("Hello, " + fullNameOfPerson);
		console.log("You are in " + ageOfPerson + " years old");
		console.log("You lived in " + locationOfPerson);
	}

	identityPrompt();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/



	//second function here:

	function topFavoriteBand(){
		let bandName1 = "A Day To Remember"
		let bandName2 =  "A Rocket To The Moon"
		let bandName3 = "Bring Me The Horizon"
		let bandName4 = "Foster The People"
		let bandName5 =  "Sleeping With Sirens"


		console.log("1. " + bandName1);
		console.log("2. " + bandName2);
		console.log("3. " + bandName3);
		console.log("4. " + bandName4);
		console.log("5. " + bandName5);
	}

	topFavoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topFavoriteMovie(){
		let movieName1 = "Shazam"
		let movieName2 =  "Scream VI"
		let movieName3 = "Inside"
		let movieName4 = "Moving On"
		let movieName5 =  "Creed III"

		let scoreRottenTomatoe1 = "90%"
		let scoreRottenTomatoe2 = "23%"
		let scoreRottenTomatoe3 = "34%"
		let scoreRottenTomatoe4 = "64%"
		let scoreRottenTomatoe5 = "78%"

		console.log("1. " + movieName1);
		console.log("Rotten Tomatoes Rating: " + scoreRottenTomatoe1);
		console.log("2. " + movieName2);
		console.log("Rotten Tomatoes Rating: " + scoreRottenTomatoe2);
		console.log("3. " + movieName3);
		console.log("Rotten Tomatoes Rating: " + scoreRottenTomatoe3)
		console.log("4. " + movieName4);
		console.log("Rotten Tomatoes Rating: " + scoreRottenTomatoe4);
		console.log("5. " + movieName5);
		console.log("Rotten Tomatoes Rating: " + scoreRottenTomatoe5);
	}

	topFavoriteMovie();

/*		function topFavoritMovie(){
		let movieName = [""];
		console.log(topFavoritMovie);
	}

	topFavoritMovie();
*/


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = alert("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	
	console.log("Please Enter yor friends name!" + friend2);
console.log("Please Enter yor friends name!" + friend3);
};


/*alert("Please Enter yor friends name!");*/

printFriends();